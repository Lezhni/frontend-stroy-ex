var max_height = 0;
$(document).ready(function() {

	$('.top-panel-search').on('click', showSearchPanel);

	if ($('.license-single, .award-single, .review-single').length != 0) {
		$('.license-single a, .award-single a, .review-single .review-original').magnificPopup({
			type: 'image',
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			}
		});
	}
	if ($('.gallery-wrapper').length != 0) {
		$('.gallery-wrapper').slick({
			infinite: true,
		    slidesToShow: 1,
		    slidesToScroll: 1,
		    arrows: true,
		    prevArrow: '<a href="#" class="prev-link"></a>',
		    nextArrow: '<a href="#" class="next-link"></a>',
		    onAfterChange: function(e, i) {
		    	$('.modal-gallery-title').text($('.cat-example-single').eq(i).find('img').data('title'));
		    	$('.modal-gallery-desc').html($('.cat-example-single').eq(i).next('.modal-desc').html());
		    }
		});
	}
	if ($('.index-slider').length != 0) {
		$('.index-slider').slick({
			infinite: true,
		    slidesToShow: 1,
		    slidesToScroll: 1,
		    autoplay:true,
		    autoplaySpeed: 3000,
		    dots: true,
		    arrows: true,
		    prevArrow: '<a href="#" class="slider-prev"></a>',
		    nextArrow: '<a href="#" class="slider-next"></a>',
		});
	}

	$('.project-square-input').ionRangeSlider({
		min: 30,
		max: 250,
		type: 'double',
		grid: true,
		grid_num: 2,
		hide_min_max: true,
		hide_from_to: true,
		onChange: function(data) {
			$('.square-block-from').val(data.from);
			$('.square-block-to').val(data.to);
		}	
	});
	$('.square-block-from, .square-block-to').keyup(function() {
		$('.project-square-input').data("ionRangeSlider").update({
			from: $('.square-block-from').val(),
			to: $('.square-block-to').val()
		});
	});

	var dd = 0;
	$('a.dropdown').click(function() {
		
		$('.dropdown-list').slideUp();

		if (dd === 0) {
			dd = 1;
			$(this).next('.dropdown-list').width($(this).width()).slideDown();
		} else {
			$(this).next('.dropdown-list').slideDown();
		}

		return false;
	});
	$('.dropdown-list a').click(function() {

		$(this).closest('.dropdown-list').prev('a.dropdown').text($(this).text());
		console.log($(this).text());
		$(this).closest('.dropdown-list').slideUp();

		return false;
	});

	$('.radio').click(function() {

		$(this).closest('.radio-wrapper').find('.radio').removeClass('checked');
		$(this).closest('.radio-wrapper').find('input[type=radio]').removeAttr('checked');
		$(this).addClass('checked');
		$(this).next('input[type=radio]').attr('checked', 'checked');
	});

	$('.checkbox').click(function() {

		if ($(this).hasClass('checked')) {
			$(this).removeClass('checked');
			$(this).next('input[type=checkbox').removeAttr('checked');
		} else {
			$(this).addClass('checked');
			$(this).next('input[type=checkbox').attr('checked', 'checked');
		}
	});

	$('.anketa-form').submit(function() {

		$.magnificPopup.open({
			items: {
				src: '#form-error',
				type: 'inline'
			}
		});

		return false;
	});

	$('.cat-examples-list:not(.not-gallery) .cat-example-single').click(function() {

		var index = $(this).children('img').data('modal-id');
		var title = $(this).children('img').data('title');
		var desc = $(this).next('.modal-desc').html();

		$.magnificPopup.open({
			items: {
				src: '#modal-gallery',
				type: 'inline'
			},
			callbacks: {

				open: function() {
					$('.gallery-wrapper').slickGoTo(index);
					$('.modal-gallery-title').text(title);
					$('.modal-gallery-desc').html(desc);
				}
			}
		});
		return false;
	});

	if ($('.license-single').length != 0) {
		$('.license-single').each(function() {
			if ($(this).height() > max_height) {
				max_height = $(this).height();
			}
		});
		$('.license-single').height(max_height);
	}
	if ($('.award-single').length != 0) {
		$('.award-single').each(function() {
			if ($(this).height() > max_height) {
				max_height = $(this).height();
			}
		});
		$('.award-single').height(max_height);
	}

	$('.vacancy-single-title').on('click', function() {
		if (!$(this).hasClass('vacancy-opened')) {
			$('.vacancy-single-title').removeClass('vacancy-opened').next('.vacancy-single-desc').slideUp();
			$(this).addClass('vacancy-opened').next('.vacancy-single-desc').slideDown();
		} else {
			$(this).removeClass('vacancy-opened').next('.vacancy-single-desc').slideUp();
		}
		return false;
	});

	$('.faq-category-link').on('click', function() {
		if (!$(this).hasClass('faq-opened')) {
			$('.faq-category-link').removeClass('faq-opened').next('.faq-category-answer').slideUp();
			$(this).addClass('faq-opened').next('.faq-category-answer').slideDown();
		} else {
			$(this).removeClass('faq-opened').next('.faq-category-answer').slideUp();
		}
		return false;
	});

	if ($('input[type=file]').length != 0) {
		$('input[type=file]').jInputFile();
	}

	/* Adaptive */

	if ($(window).width() < 1220) {
		$('.award-single, .license-single').next('.vertical-divider').remove();
		$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
		$('.award-single:nth-child(5n), .license-single:nth-child(5n)').next('.vertical-divider').remove();
	}
	if ($(window).width() > 1220 && $(window).width() < 1425) {
		$('.award-single, .license-single').next('.vertical-divider').remove();
		$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
		$('.award-single:nth-child(7n), .license-single:nth-child(7n)').next('.vertical-divider').remove();
	}
	if ($(window).width() > 1425 && $(window).width() < 1695) {
		$('.award-single, .license-single').next('.vertical-divider').remove();
		$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
		$('.award-single:nth-child(9n), .license-single:nth-child(9n)').next('.vertical-divider').remove();
	}
	if ($(window).width() > 1695) {
		$('.award-single, .license-single').next('.vertical-divider').remove();
		$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
		$('.award-single:nth-child(11n), .license-single:nth-child(11n)').next('.vertical-divider').remove();
	}

	$(window).resize(function() {

		if ($(window).width() < 1220) {
			$('.award-single, .license-single').next('.vertical-divider').remove();
			$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
			$('.award-single:nth-child(5n), .license-single:nth-child(5n)').next('.vertical-divider').remove();
		}
		if ($(window).width() > 1220 && $(window).width() < 1425) {
			$('.award-single, .license-single').next('.vertical-divider').remove();
			$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
			$('.award-single:nth-child(7n), .license-single:nth-child(7n)').next('.vertical-divider').remove();
		}
		if ($(window).width() > 1425 && $(window).width() < 1695) {
			$('.award-single, .license-single').next('.vertical-divider').remove();
			$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
			$('.award-single:nth-child(9n), .license-single:nth-child(9n)').next('.vertical-divider').remove();
		}
		if ($(window).width() > 1695) {
			$('.award-single, .license-single').next('.vertical-divider').remove();
			$('.award-single, .license-single').after('<div class="vertical-divider"></div>');
			$('.award-single:nth-child(11n), .license-single:nth-child(11n)').next('.vertical-divider').remove();
		}
	});
});


function showSearchPanel() {
	thisIcon = $('.top-panel-search');
	thisForm = $('.top-panel-search-form');
	if (!thisIcon.hasClass('search-panel-opened')) {
		thisIcon.addClass('search-panel-opened');
		thisForm.show();
	} else {
		thisIcon.removeClass('search-panel-opened');
		thisForm.hide();
	}
	
	return false;
}